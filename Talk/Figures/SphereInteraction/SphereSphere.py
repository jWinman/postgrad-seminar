import numpy as np
from mayavi import mlab

fig = mlab.figure(bgcolor=(1, 1, 1))

x1, y1, z1 = np.array([0]), np.array([0]), np.array([0])
x2, y2, z2 = np.array([0.8]), np.array([0]), np.array([0])

mlab.points3d(x1, y1, z1, np.ones(1), resolution=40, color=(1, 0, 0), scale_factor=1., opacity=0.55)
mlab.points3d(x2, y2, z2, np.ones(1), resolution=40, color=(0, 0, 1), scale_factor=1., opacity=0.55)

mlab.show()
