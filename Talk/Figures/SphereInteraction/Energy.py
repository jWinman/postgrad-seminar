import numpy as np
import matplotlib.pyplot as plt

quadratic = lambda x: x**2
x = np.linspace(0, 10, 1000)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

ax.plot(x, quadratic(x), "b")
ax.set_ylim(0, 100)
ax.set_xlim(0, 10)
ax.set_ylabel("Energy $U$")
ax.set_xlabel("overlap $\delta$")

ax.set_xticklabels([])
ax.set_yticklabels([])

fig.savefig("SphereEnergy.pdf")
